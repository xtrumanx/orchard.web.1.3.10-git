﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Fields.Fields;
using Orchard.Fields.Settings;
using Orchard.Localization;

namespace Orchard.Fields.Drivers {
    public class LinkFieldDriver : ContentFieldDriver<LinkField> {
        public IOrchardServices Services { get; set; }
        private const string TemplateName = "Fields/Link";

        public LinkFieldDriver(IOrchardServices services) {
            Services = services;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        private static string GetPrefix(ContentField field, ContentPart part) {
            return part.PartDefinition.Name + "." + field.Name;
        }

        protected override DriverResult Display(ContentPart part, LinkField field, string displayType, dynamic shapeHelper) {
            var settings = field.PartFieldDefinition.Settings.GetModel<LinkFieldSettings>();

            return ContentShape("Fields_Link",
                                () => shapeHelper.Fields_Link().ContentField(field).ContentPart(part).Settings(settings));
        }

        protected override DriverResult Editor(ContentPart part, LinkField field, dynamic shapeHelper) {
            return ContentShape("Fields_Link_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: field, Prefix: GetPrefix(field, part)));
        }

        protected override DriverResult Editor(ContentPart part, LinkField field, IUpdateModel updater, dynamic shapeHelper) {
            if (updater.TryUpdateModel(field, GetPrefix(field, part), null, null)) {
                var settings = field.PartFieldDefinition.Settings.GetModel<LinkFieldSettings>();
                if (!settings.OptionalUrl && string.IsNullOrWhiteSpace(field.Url)) {
                    updater.AddModelError(GetPrefix(field, part), T("Url is required"));
                }
                else if (!string.IsNullOrWhiteSpace(field.Url) && !Uri.IsWellFormedUriString(field.Url, UriKind.RelativeOrAbsolute)) {
                    updater.AddModelError(GetPrefix(field, part), T("{0} is an invalid url.", field.Url));
                }
                else if (settings.LinkTitleMode == LinkTitleMode.RequiredTitle && string.IsNullOrWhiteSpace(field.Title)) {
                    updater.AddModelError(GetPrefix(field, part), T("Title is required."));
                }
            }

            return Editor(part, field, shapeHelper);
        }

        protected override void Importing(ContentPart part, LinkField field, ImportContentContext context) {
            field.Text = context.Attribute(field.FieldDefinition.Name + "." + field.Name, "Text");
            field.Title = context.Attribute(field.FieldDefinition.Name + "." + field.Name, "Title");
            field.Url = context.Attribute(field.FieldDefinition.Name + "." + field.Name, "Url");
        }

        protected override void Exporting(ContentPart part, LinkField field, ExportContentContext context) {
            context.Element(field.FieldDefinition.Name + "." + field.Name).SetAttributeValue("Text", field.Text);
            context.Element(field.FieldDefinition.Name + "." + field.Name).SetAttributeValue("Title", field.Title);
            context.Element(field.FieldDefinition.Name + "." + field.Name).SetAttributeValue("Url", field.Url);
        }

        protected override void Describe(DescribeMembersContext context) {
            context
                .Member("Text", typeof(string), T("Text"), T("The text of the link."))
                .Member("Title", typeof(string), T("Title"), T("The title of the link."))
                .Member("Url", typeof(string), T("Url"), T("The url of the link."));
        }
    }
}
