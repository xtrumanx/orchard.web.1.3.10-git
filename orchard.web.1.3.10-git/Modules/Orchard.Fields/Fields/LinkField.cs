﻿using System;
using System.Globalization;
using Orchard.ContentManagement;
using Orchard.ContentManagement.FieldStorage;

namespace Orchard.Fields.Fields {
    public class LinkField : ContentField {

        public string Text
        {
            get { return Storage.Get<string>("Text"); }
            set { Storage.Set("Text", value ?? String.Empty); }
        }

        public string Title {
            get { return Storage.Get<string>("Title"); }
            set { Storage.Set("Title", value ?? String.Empty); }
        }

        public string Url
        {
            get { return Storage.Get<string>("Url"); }
            set { Storage.Set("Url", value ?? String.Empty); }
        }

        public string Target {
            get { return Storage.Get<string>("Target"); }
            set { Storage.Set("Target", value ?? String.Empty); }
        }
    }
}
