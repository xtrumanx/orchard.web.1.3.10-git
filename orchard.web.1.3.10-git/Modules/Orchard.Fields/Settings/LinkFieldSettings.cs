﻿namespace Orchard.Fields.Settings {
    public class LinkFieldSettings {
        public string Hint { get; set; }
        public bool OptionalUrl { get; set; }
        public TargetMode TargetMode { get; set; }
        public LinkTitleMode LinkTitleMode { get; set; }
        public string StaticTitle { get; set; }

        public LinkFieldSettings() {
            TargetMode = TargetMode.Default;
            LinkTitleMode = LinkTitleMode.OptionalTitle;
        }
    }

    public enum TargetMode {
        Default,
        NewWindow,
        Parent,
        Top,
        UserChoice
    }

    public enum LinkTitleMode {
        OptionalTitle,
        RequiredTitle,
        StaticTitle,
        NoTitle
    }
}
